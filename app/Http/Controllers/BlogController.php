<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;

class BlogController extends Controller
{
    //
    public function index()
    {
        $blog = Blog::latest()->paginate(10);
        return view('blog', compact('blog'));
    }

    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            // 'image' => 'required|image|mimes:png,jpg,jpeg',
            'title' => 'required',
            'content' => 'required'
        ]);

        // $image = $request->file('image');
        // $image->storeAs('public/blogs', $image->hashName());

        $blog = Blog::create([
            // 'image' => $image->hashName(),
            'title' => $request->title,
            'content' => $request->content
        ]);

        if ($blog) {
            return redirect()->route('index')->with(['success' => 'Data Berhasil Disimpan!']);
        } else {
            return redirect()->route('index')->with(['error' => 'Data Gagal Disimpan!']);
        }
    }

    // public function destroy
}
